// Main.java
public class Main {
    public static void main(String[] args) {
        // Creating objects of Person class
        Person person1 = new Person("John", 25);
        person1.introduce();

        Person person2 = new Person("Jane", 30, "123 Main St", "555-1234", "jane@example.com");
        person2.introduce();
        person2.setAddress("456 Elm St");
        person2.setContactInfo("555-5678", "jane.new@example.com");

        // Creating objects of Car class
        Car car1 = new Car("Toyota", "Camry");
        car1.startEngine();

        Car car2 = new Car("Honda", "Civic", 2022, "Red", 25000.0);
        car2.startEngine();
        car2.setColor("Blue");
        car2.displayPrice();
    }
}
