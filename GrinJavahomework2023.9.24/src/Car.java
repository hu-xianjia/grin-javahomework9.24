// Car.java
public class Car {
    // Properties
    private String brand;
    private String model;
    private int year;
    private String color;
    private double price;

    // Constructors
    public Car(String brand, String model) {
        this.brand = brand;
        this.model = model;
    }

    public Car(String brand, String model, int year, String color, double price) {
        this.brand = brand;
        this.model = model;
        this.year = year;
        this.color = color;
        this.price = price;
    }

    // Methods
    public void startEngine() {
        System.out.println(brand + " " + model + " engine started.");
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void displayPrice() {
        System.out.println("The price of the " + brand + " " + model + " is $" + price);
    }
}
