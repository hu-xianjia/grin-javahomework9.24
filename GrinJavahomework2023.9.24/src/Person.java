
// Person.java
public class Person {
    // Properties
    private String name;
    private int age;
    private String address;
    private String phoneNumber;
    private String email;

    // Constructors
    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Person(String name, int age, String address, String phoneNumber, String email) {
        this.name = name;
        this.age = age;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }

    // Methods
    public void introduce() {
        System.out.println("My name is " + name + ". I am " + age + " years old.");
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setContactInfo(String phoneNumber, String email) {
        this.phoneNumber = phoneNumber;
        this.email = email;
    }
}
